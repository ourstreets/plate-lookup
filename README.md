# Modular NodeJS vehicle citation lookup library

This is a collection of NodeJS libraries for looking up citation information about vehicles using
their license plate. It consists of the following packages:

 * [`@ourstreets/plate-lookup-alx`](./packages/alx) - Alexandria, VA
 * [`@ourstreets/plate-lookup-atx`](./packages/atx) - Austin, TX
 * [`@ourstreets/plate-lookup-dc`](./packages/dc) - Washington, DC
 * [`@ourstreets/plate-lookup-dspayments`](./packages/dspayments) - Duncan Solutions Payment System (powers ALX, FFX, & PGH)
 * [`@ourstreets/plate-lookup-etimspayments`](./packages/etimspayments) - eTIMS Payment System (powers DC)
 * [`@ourstreets/plate-lookup-ffx`](./packages/ffx) - Fairfax, VA
 * [`@ourstreets/plate-lookup-nyc`](./packages/nyc) - New York, NY
 * [`@ourstreets/plate-lookup-pgh`](./packages/pgh) - Pittsburgh, PA
 * [`@ourstreets/plate-lookup-phl`](./packages/phl) - Philadelphia, PA
 * [`@ourstreets/plate-lookup-sea`](./packages/sea) - Seattle, WA

[`@ourstreets/plate-lookup`](./packages/meta) is also available, exporting all of the above by the class names in
their docs.

## This library is a pre-release. until v1.0.0, minor version bumps will likely include breaking changes

## Quick start
Install with `npm i @ourstreets/plate-lookup`

```javascript
const { DcLookup } = require('@ourstreets/plate-lookup')

const dc = new DcLookup();
dc.lookup('DC', '69').then(console.log)
```

### Modular
Or to use the modular packages install with `npm i @ourstreets/plate-lookup-dc`

```javascript
const DcLookup = require('@ourstreets/plate-lookup')

const dc = new DcLookup();
dc.lookup('DC', '69').then(console.log)
```

## Thanks
Special thanks to all the developers who've opensourced or contributed plate lookup scripts:
 * [Brian Howland](https://twitter.com/bdhowald)'s [@HowsMyDrivingNY](https://twitter.com/howsmydrivingny) - https://github.com/bdhowald/hows_my_driving
 * [Aaron Bauman](https://twitter.com/aaronbauman)'s [@HowsMyDrivingPA](https://twitter.com/howsmydrivingpa) - https://github.com/aaronbauman/tagbot
 * [Glen Buhlmann](https://twitter.com/GlenBikes)'s [@HowsMyDrivingWA](https://twitter.com/howsmydrivingwa) - https://github.com/GlenBikes/HowsMyDriving-Seattle
 * [Rémy Greinhofer](https://github.com/rgreinho) for the Austin, TX initial implementation - https://gitlab.com/snippets/1901561
 * [Daniel Schep](https://twitter.com/_schep)'s [@HowsMyDrivingDC](https://twitter.com/howsmydrivingdc) - https://github.com/dschep/hows-my-driving-dc
