const TicketLookupDotComLookup = require("@ourstreets/plate-lookup-ticketlookup.com");

/**
 * Pittsburgh Plate lookup service
 * @see {@link ../dspayments|@ourstreets/plate-lookup-dspayments} for more info.
 */
class RvaLookup extends TicketLookupDotComLookup {
  /**
   * Create an instance of the Richmond VA Plate lookup service
   */
  constructor() {
    super("richmond");
  }
}

module.exports = RvaLookup;
