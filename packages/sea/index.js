const soap = require("soap");

/**
 * Seattle Plate lookup service
 *
 * Uses an undocumented SOAP endpoint on the Seattle court service's website.
 */
class SeaLookup {
  /**
   * Initialize the SOAP Client
   * @async
   * @returns {undefined}
   */
  async initialize() {
    if (this.client) return;
    this.client = await new Promise((resolve, reject) =>
      soap.createClient(
        "https://web6.seattle.gov/Courts/ECFPortal/JSONServices/ECFControlsService.asmx?wsdl",
        (err, client) => (err ? reject(err) : resolve(client))
      )
    );
  }

  /**
   * Lookup a plate
   * @async
   * @param {string} state - The state of the license plate (2 character state code)
   * @param {string} number - The plate numbers and letters
   * @returns {Array<Object>} - An array of citation objects
   */
  async lookup(state, number) {
    await this.initialize();
    const vehicleNumbers = await new Promise((resolve, reject) =>
      this.client.GetVehicleByPlate(
        { Plate: number, State: state },
        (err, result) =>
          err
            ? reject(err)
            : resolve(
                JSON.parse(JSON.parse(result.GetVehicleByPlateResult).Data)
              )
      )
    );
    const citations = [];
    const citationIds = new Set();
    for (const { VehicleNumber } of vehicleNumbers) {
      const vehicleCitations = await new Promise((resolve, reject) =>
        this.client.GetCitationsByVehicleNumber(
          { VehicleNumber },
          (err, result) =>
            err
              ? reject(err)
              : resolve(
                  JSON.parse(
                    JSON.parse(result.GetCitationsByVehicleNumberResult).Data
                  )
                )
        )
      );
      for (const citation of vehicleCitations) {
        if (!citationIds.has(citation.Citation)) {
          citationIds.add(citation.Citation);
          citations.push(citation);
        }
      }
    }
    return citations;
  }
}

module.exports = SeaLookup;
