const fs = require("fs");
const fetch = require("node-fetch");
const simplify = require("@turf/simplify");
const flatten = require("@turf/flatten");
const polygonToLine = require("@turf/polygon-to-line").default;
const pointToLineDistance = require("@turf/point-to-line-distance").default;

/**
 * Get a simplified boundary as GeoJSON of an OSM relation
 * @async
 * @param {number} osmRelationId - The OSM Relation ID to generate GeoJSON for
 * @returns {Promise<Object>} simplified GeoJSON of the OSM relation
 */
module.exports.getBoundary = async osmRelationId => {
  const resp = await fetch(
    `http://polygons.openstreetmap.fr/get_geojson.py?id=${osmRelationId}&params=0`
  );
  const geometry = await resp.json();
  const feature = {
    type: "Feature",
    properties: { osmRelationId },
    geometry
  };

  simplify(feature, { mutate: true, tolerance: 0.001 });

  return geometry;
};

/**
 * Get all simplified boundaries for the jurisdictions @ourstreets/plate-lookup supports and write
 * them to disk in a folder named `regions`
 * @async
 * @returns {Promise<undefined>} resolves when done
 */
module.exports.getAllBoundaries = async () => {
  const regions = [
    ["alx", 206637],
    ["dc", 162069],
    ["atx", 113314],
    ["pgh", 188553],
    ["phl", 188022],
    ["nyc", 175905],
    ["ffx", 945043],
    ["sea", 237385]
  ];

  for (const [regionName, osmRelationId] of regions) {
    const geometry = await module.exports.getBoundary(osmRelationId);

    if (!fs.existsSync("regions")) {
      fs.mkdirSync("regions");
    }
    fs.writeFileSync(
      `regions/${regionName}.json`,
      JSON.stringify(flatten(geometry))
    );
  }
};

/**
 * Get nearby regions to a point.
 * @async
 * @param {object} point - GeoJSON representation of a point
 * @param {object} options - buffer, units, and regions
 * @returns {Promise<Object>} simplified GeoJSON of the OSM relation
 */
module.exports.getNearbyRegions = (point, options) => {
  const buffer = (options && options.buffer) || 100;
  const units = (options && options.units) || "kilometers";
  const regions = (options && options.regions) || [
    "alx",
    "dc",
    "atx",
    "pgh",
    "phl",
    "nyc",
    "ffx",
    "sea"
  ];
  return regions
    .map(region =>
      JSON.parse(fs.readFileSync(`regions/${region}.json`).toString())
    )
    .map((region, i) => {
      let distance = NaN;
      // FIXME - should also check if point is inside polygon, so for now only works with large
      // enough buffer with respect to the region boundary
      for (const polygon of region.features) {
        if (isNaN(distance)) {
          distance = pointToLineDistance(point, polygonToLine(polygon), {
            units
          });
        } else {
          distance = Math.min(
            distance,
            pointToLineDistance(point, polygonToLine(polygon), { units })
          );
        }
      }
      console.log(regions[i], distance);
      return [regions[i], distance];
    })
    .filter(([, distance]) => distance < buffer)
    .sort((a, b) => a.distance - b.distance);
};
