const { getBoundary } = require(".");
const { flatten } = require("@turf/turf");

getBoundary(process.argv[2])
  .then(flatten)
  .then(JSON.stringify)
  .then(console.log);
