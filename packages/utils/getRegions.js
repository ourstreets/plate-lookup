const { getBoundary } = require(".");
const { buffer, flatten, union, feature, featureCollection } = require("@turf/turf");

Promise.all(process.argv.slice(2).map(id => getBoundary(id)))
  .then(features => features.map(feat => buffer(feature(feat.geometries[0]), .05)))
  .then(features => features.reduce((acc, val) => acc ? union(acc, val) : val))
  .then(flatten)
  .then(({features: [{geometry}]}) => geometry)
  .then(JSON.stringify)
  .then(console.log)
  .catch(console.error);
