const fs = require("fs");
const puppeteer = require("puppeteer");
const tesseract = require("tesseract.js");

const screenshotDOMElement = require("./screenshotDOMElement.js");

/**
 * Lookup service class for municipalities using etimspayments.
 *
 * Uses [Puppeteer](https://github.com/puppeteer/puppeteer)
 * and
 * [Tesseract.js](https://tesseract.projectnaptha.com/)
 * to load etimspayments powered ticket payments systems to get
 * unpaid citations.
 */
class EtimspaymentsLookup {
  /**
   * Create an instance of the etimspayments Plate lookup service
   * @param {string} url - The URL of the etimspayments instance
   * @param {object} [browser] - Puppeeteer browser instance
   * @param {object} [tesseractWorker] - Tesseract worker instance
   */
  constructor(url, browser, tesseractWorker) {
    this.url = url;
    this.browser = browser;
    this.tesseractWorker = tesseractWorker;
  }
  /**
   * Initialize the browser and tesseractWorker if needed
   * @async
   */
  async initialize() {
    if (!this.browser) {
      this.browser = await puppeteer.launch({
        headless: true,
        args: ["--no-sandbox", "--disable-gpu", "--single-process"]
      });
    }
    if (!this.tesseractWorker) {
      this.tesseractWorker = tesseract.createWorker({ langPath: __dirname });
      await this.tesseractWorker.load();
      await this.tesseractWorker.loadLanguage("eng");
      await this.tesseractWorker.initialize("eng");
    }
  }
  /**
   * Close the puppeteer browser and terminate the tesseractWorker
   * @async
   */
  async teardown() {
    await this.browser.close();
    await this.tesseractWorker.terminate();
  }
  /**
   * Lookup a plate
   * @async
   * @param {string} state - The state of the license plate (2 character state code)
   * @param {string} number - The plate numbers and letters
   * @param {object} options - the only option currently is the `retryCaptcha` key. specify an
   * integer to retry on captcha errors
   * @returns {Array<Object>} - An array of citation objects
   */
  async lookup(state, number, options) {
    // initialize
    await this.initialize();

    // create load payments page
    const page = await this.browser.newPage();
    await page.setViewport({ height: 768, width: 1024 });
    await page.goto(this.url, {
      waitUntil: ["domcontentloaded", "networkidle0"]
    });

    // Enter license plate number
    await page.type("[name=plateNumber]", number);

    // Set state
    await page.evaluate(state => {
      document.querySelector("[name=statePlate]").value = state;
    }, state);

    // solve the captcha >:D
    await screenshotDOMElement(page, {
      path: "/tmp/captcha.png",
      selector: "#captcha",
      padding: 4
    });
    const {
      data: { text }
    } = await this.tesseractWorker.recognize("/tmp/captcha.png");
    const captcha = text.replace(/\D/g, "");
    await page.type("[name=captchaSText]", captcha);
    fs.unlinkSync("/tmp/captcha.png");

    // avoid to timeout waitForNavigation() after click()
    await Promise.all([page.waitForNavigation(), page.keyboard.press("Enter")]);

    // parse the results
    const error = await page.evaluate(() => {
      if (document.querySelector("[name=selectForm]") === null) {
        return (
          document.querySelector(".error") &&
          document.querySelector(".error").textContent
        );
      }
    });
    if (error && error.match && error.match(/Please enter the characters/)) {
      if (options && options.retry > 0) {
        return this.lookup(state, number, { retry: options.retry - 1 });
      }
      throw Error("captcha error");
    } else if (
      error &&
      error.match &&
      (error.match(/The Plate entered has a balance of .0.00/) ||
        error.match(/Plate is not found/))
    ) {
      return {
        total: 0,
        tickets: [],
        booted: false
      };
    } else if (error) {
      throw Error(error);
    }

    const total = await page.evaluate(() => {
      const totalInput = document.querySelector("input[name=totalAmount]");
      if (totalInput) {
        return totalInput.value.replace("$", "");
      }
      return Number(
        document
          .querySelector("[name=selectForm]")
          .textContent.match(
            /(The total of all your citations and fees is:|You have a total of \d+\sticket\(s\) on your account in the amount of) \$(\d+\.\d+)/
          )[2]
      );
    });

    const html = await page.evaluate(() => document.body.innerHTML);
    const [tickets, booted] = await page.evaluate(() => {
      const tickets = [];
      if (document.querySelector("li.error")) {
        for (const row of document.querySelectorAll(
          'table[width="770"][cellpadding="2"][cellspacing="0"][border="0"] tr + tr + tr ~ tr'
        )) {
          tickets.push(
            Array.from(
              row.querySelectorAll(
                ":not(:first-child):not(:nth-child(6)):not(:nth-child(7))"
              )
            ).map(el => el.textContent)
          );
        }
      } else {
        for (const row of document.querySelectorAll(
          'table[width="100%"][cellpadding="2"][cellspacing="0"][border="0"] table tbody tr:not(:first-child)'
        )) {
          if (row.children.length !== 5) continue;
          tickets.push(Array.from(row.children).map(el => el.textContent));
        }
      }
      return [tickets, Boolean(document.querySelector("li.error"))];
    });

    return {
      total,
      tickets: tickets.map(
        ([ticketNumber, date, violation, location, value]) => ({
          ticketNumber,
          date,
          violation,
          location,
          value
        })
      ),
      booted
    };
  }
}

module.exports = EtimspaymentsLookup;
