const DsPaymentsLookup = require("@ourstreets/plate-lookup-dspayments");

/**
 * Pittsburgh Plate lookup service
 * @see {@link ../dspayments|@ourstreets/plate-lookup-dspayments} for more info.
 */
class PghLookup extends DsPaymentsLookup {
  /**
   * Create an instance of the Pittsburgh Plate lookup service
   */
  constructor() {
    super("https://dspayments.com/pittsburgh");
  }
}

module.exports = PghLookup;
