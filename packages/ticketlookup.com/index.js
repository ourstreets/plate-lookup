const querystring = require("querystring");
const fromPairs = require("lodash.frompairs");
const nodeFetch = require("node-fetch");
const tough = require("tough-cookie");
const fetch = require("fetch-cookie")(nodeFetch);
const { JSDOM } = require("jsdom");

/**
 * ticketlookup.com Plate lookup service
 *
 * Uses the public inqury page with cookies and JSDOM.
 */
class TicketLookupDotComLookup {
  constructor(id) {
    this.searchUrl = "http://www.ticketlookup.com/RequestPlate.aspx";
    this.portalUrl = `http://www.ticketlookup.com/default.aspx?id=${id}`;
  }
  /**
   * Lookup a plate
   * @async
   * @param {string} state - The state of the license plate (2 character state code)
   * @param {string} number - The plate numbers and letters
   * @returns {Array<Object>} - An array of citation objects
   */
  async lookup(state, number) {
    // GET portal URL
    let resp = await fetch(this.portalUrl);
    let dom = new JSDOM(await resp.text());

    // GET search URL
    resp = await fetch(this.searchUrl);
    dom = new JSDOM(await resp.text());

    // extract viewstate values
    const __VIEWSTATE = dom.window.document.querySelector("#__VIEWSTATE").value;
    const __VIEWSTATEGENERATOR = dom.window.document.querySelector(
      "#__VIEWSTATEGENERATOR"
    ).value;
    const __EVENTVALIDATION = dom.window.document.querySelector(
      "#__EVENTVALIDATION"
    ).value;

    // POST to search URL
    resp = await fetch(this.searchUrl, {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: querystring.encode({
        __EVENTTARGET: "",
        __EVENTARGUMENT: "",
        __VIEWSTATE,
        __VIEWSTATEGENERATOR,
        __EVENTVALIDATION,
        txtPlate: number,
        btnSubmit: "Submit"
      })
    });
    dom = new JSDOM(await resp.text());

    // Parse citations
    const columns = Array.from(
      dom.window.document.querySelectorAll(
        "table#GridView1 tbody th:not(:first-child)"
      )
    ).map(({ textContent }) => textContent);
    const citations = [];
    for (const row of Array.from(
      dom.window.document.querySelectorAll(
        "table#GridView1 tbody tr:not(:last-child):not(:first-child)"
      )
    )) {
      citations.push(
        fromPairs(
          columns.map((col, i) => [col, row.cells[i + 1].textContent.trim()])
        )
      );
    }
    return citations;
  }
}

module.exports = TicketLookupDotComLookup;
