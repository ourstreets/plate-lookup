const TicketLookupDotComLookup = require(".");

describe("ticketlooukp.com Lookup", () => {
  it("return citations with the right keys for VA:uyl2254", async () => {
    const ticketlookup = new TicketLookupDotComLookup("richmond");
    const citations = await ticketlookup.lookup("VA", "uyl2254");
    expect(Object.keys(citations[0])).toEqual([
      "Citation",
      "Plate",
      "State",
      "Issued",
      "Status",
      "Last Date",
      "Violation",
      "Due"
    ]);
  }, 60000);

  it("return first know citation for VA:uyl2254", async () => {
    const ticketlookup = new TicketLookupDotComLookup("richmond");
    const citations = await ticketlookup.lookup("VA", "uyl2254");
    expect(citations[0]).toEqual({
      Citation: "|01330167|",
      Plate: "UYL2254",
      State: "VA",
      Issued: "02/29/2020",
      Status: "OPEN",
      "Last Date": "/  /",
      Violation: "PKG TOW ZONE",
      Due: "60.00"
    });
  }, 60000);
});
