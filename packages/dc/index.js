const EtimspaymentsLookup = require("@ourstreets/plate-lookup-etimspayments");

/**
 * DC Plate lookup service
 * @see {@link ../etimspayments|@ourstreets/plate-lookup-etimspayments} for more info.
 * @example
 * const DcLookup = require("@ourstreets/plate-lookup-dc");
 * const dc = new DcLookup();
 * dc.lookup("DC", "69").then(console.log);
 */
class DcLookup extends EtimspaymentsLookup {
  /**
   * Create an instance of the DC Plate lookup service
   * @param {object} [browser] - Puppeeteer browser instance
   * @param {object} [tesseractWorker] - Tesseract worker instance
   */
  constructor(browser, tesseractWorker) {
    super(
      "https://prodpci.etimspayments.com/pbw/include/dc_parking/input.jsp",
      browser,
      tesseractWorker
    );
  }
}

module.exports = DcLookup;
