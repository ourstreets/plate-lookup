const DcLookup = require(".");

describe("DC Lookup", () => {
  let dc;
  beforeAll(async () => {
    dc = new DcLookup();
    await dc.initialize();
  });
  afterAll(async () => {
    await dc.teardown();
  });
  it("return 29(limit) tix for for DC:69", async () => {
    const citations = await dc.lookup("DC", "69");
    expect(citations.tickets.length).toEqual(29);
  });
  it("return right keys for DC:69", async () => {
    const citations = await dc.lookup("DC", "69", { retry: 10 });
    expect(Object.keys(citations.tickets[0])).toEqual([
      "ticketNumber",
      "date",
      "violation",
      "location",
      "value"
    ]);
  });
});
