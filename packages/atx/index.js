const querystring = require("querystring");
const fromPairs = require("lodash.frompairs");
const nodeFetch = require("node-fetch");
const tough = require("tough-cookie");
const fetch = require("fetch-cookie")(
  nodeFetch,
  new tough.CookieJar(undefined, { looseMode: true })
);
const { JSDOM } = require("jsdom");

const portalUrl = "https://www.austintexas.gov/AmcPublicInquiry/pubportal.aspx";
const searchUrl =
  "https://www.austintexas.gov/AmcPublicInquiry/search/vclsearch.aspx";
/**
 * Austin Plate lookup service
 *
 * Uses the public inqury page with cookies and JSDOM.
 */
class AtxLookup {
  /**
   * Lookup a plate
   * @async
   * @param {string} state - The state of the license plate (2 character state code)
   * @param {string} number - The plate numbers and letters
   * @returns {Array<Object>} - An array of citation objects
   */
  async lookup(state, number) {
    // GET portal URL
    let resp = await fetch(portalUrl);
    let dom = new JSDOM(await resp.text());

    // extract viewstate values
    let __VIEWSTATE = dom.window.document.querySelector("#__VIEWSTATE").value;
    let __VIEWSTATEGENERATOR = dom.window.document.querySelector(
      "#__VIEWSTATEGENERATOR"
    ).value;

    // POST to portal URL to get cookie for vcl search
    resp = await fetch(portalUrl, {
      redirect: "manual",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: querystring.encode({
        __EVENTTARGET: "vehicleCmd",
        __EVENTARGUMENT: "",
        __VIEWSTATE,
        __VIEWSTATEGENERATOR
      }),
      method: "POST"
    });

    // GET search URL
    resp = await fetch(searchUrl);
    dom = new JSDOM(await resp.text());

    // extract viewstate values
    __VIEWSTATE = dom.window.document.querySelector("#__VIEWSTATE").value;
    __VIEWSTATEGENERATOR = dom.window.document.querySelector(
      "#__VIEWSTATEGENERATOR"
    ).value;

    // POST to search URL
    resp = await fetch(searchUrl, {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: querystring.encode({
        __EVENTTARGET: "",
        __EVENTARGUMENT: "",
        __VIEWSTATE,
        __VIEWSTATEGENERATOR,
        searchForm$plateCtl: number.toUpperCase(),
        searchForm$stateCtl: state.toUpperCase(),
        searchForm$submitCmd: "Search"
      })
    });
    dom = new JSDOM(await resp.text());

    // Parse citations
    const columns = Array.from(
      dom.window.document.querySelectorAll("#casesCtl_gridCtl th")
    ).map(({ textContent }) => textContent);
    const citations = [];
    for (const row of Array.from(
      dom.window.document.querySelectorAll("#casesCtl_gridCtl tr:not(.gridHdr)")
    )) {
      citations.push(
        fromPairs(
          columns.map((col, i) => [col, row.cells[i].textContent.trim()])
        )
      );
    }
    return citations;
  }
}

module.exports = AtxLookup;
