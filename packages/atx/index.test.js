const AtxLookup = require(".");

describe("ATX Lookup", () => {
  it("return citations with the right keys for TX:CXC9796", async () => {
    const atx = new AtxLookup();
    const citations = await atx.lookup("TX", "CXC9796");
    expect(Object.keys(citations[0])).toEqual([
      "Case #",
      "Type",
      "Name",
      "Citation",
      "Date",
      "Time",
      "Location",
      "Balance Due",
      "Status"
    ]);
  }, 60000);

  it("return first know citation for TX:CXC9796", async () => {
    const atx = new AtxLookup();
    const citations = await atx.lookup("TX", "CXC9796");
    expect(citations[0]).toEqual({
      "Case #": "V172774",
      Type: "Parking",
      Name: "Tores, Ryan Ekileo",
      Citation: "PAY STATION RECEIPT NOT DISPLAYED",
      Date: "9/3/2014",
      Time: "08:16 AM",
      Location: "1200 25TH ST W",
      "Balance Due": "$0.00",
      Status: "TERMINATED"
    });
  }, 60000);
});
