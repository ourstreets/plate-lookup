/** @see {@link ../etimspayments|@ourstreets/plate-lookup-etimspayments} */
const EtimspaymentsLookup = require("@ourstreets/plate-lookup-etimspayments");
/** @see {@link ../dspayments|@ourstreets/plate-lookup-dspayments} */
const DspaymentsLookup = require("@ourstreets/plate-lookup-dspayments");
/** @see {@link ../dc|@ourstreets/plate-lookup-dc} */
const DcLookup = require("@ourstreets/plate-lookup-dc");
/** @see {@link ../pgh|@ourstreets/plate-lookup-pgh} */
const PghLookup = require("@ourstreets/plate-lookup-pgh");
/** @see {@link ../sea|@ourstreets/plate-lookup-sea} */
const SeaLookup = require("@ourstreets/plate-lookup-sea");
/** @see {@link ../atx|@ourstreets/plate-lookup-atx} */
const AtxLookup = require("@ourstreets/plate-lookup-atx");
/** @see {@link ../nyc|@ourstreets/plate-lookup-nyc} */
const NycLookup = require("@ourstreets/plate-lookup-nyc");
/** @see {@link ../ffx|@ourstreets/plate-lookup-ffx} */
const FfxLookup = require("@ourstreets/plate-lookup-ffx");
/** @see {@link ../alx|@ourstreets/plate-lookup-alx} */
const AlxLookup = require("@ourstreets/plate-lookup-alx");

module.exports = {
  EtimspaymentsLookup,
  DspaymentsLookup,
  DcLookup,
  PghLookup,
  SeaLookup,
  NycLookup,
  AtxLookup,
  FfxLookup,
  AlxLookup
};
