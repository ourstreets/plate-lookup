const fetch = require("node-fetch");

/**
 * NYC Plate lookup service
 *
 * Uses the [NYC Open Data API](https://opendata.cityofnewyork.us/).
 */
class NyLookup {
  /**
   * Lookup a plate
   * @async
   * @param {string} state - The state of the license plate (2 character state code)
   * @param {string} number - The plate numbers and letters
   * @returns {Object} - raw json response from API
   */
  async lookup(state, number) {
    const resp = await fetch(
      `https://data.cityofnewyork.us/resource/uvbq-3m68.json?state=${state.toUpperCase()}&plate=${number.toUpperCase()}`
    );
    return await resp.json();
  }
}

module.exports = NyLookup;
