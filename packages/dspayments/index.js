const fetch = require("fetch-cookie")(require("node-fetch"));
const { JSDOM } = require("jsdom");
const FormData = require("form-data");

/**
 * Lookup service class for municipalities using dspayments
 *
 * Uses cookies and JSDOM to interact with the form
 */
class DsPaymentsLookup {
  /**
   * Create an instance of the dspayments Plate lookup service
   * @param {string} url - The URL of the dspayments instance
   */
  constructor(url) {
    this.url = url;
  }
  /**
   * Lookup a plate
   * @async
   * @param {string} state - The state of the license plate (2 character state code)
   * @param {string} number - The plate numbers and letters
   * @returns {Array<Object>} - An array of citation objects
   */
  async lookup(state, number) {
    let resp = await fetch(this.url);
    let dom = new JSDOM(await resp.text());
    const csrfToken = dom.window.document.querySelector(
      "[name=__RequestVerificationToken]"
    ).value;
    const body = new FormData();
    body.append("__RequestVerificationToken", csrfToken);
    body.append("SearchType", "Citation");
    body.append("SearchTypeAccordian", "Citation");
    body.append("CitationNumber", "");
    body.append("Plate", number);
    body.append("PlateStateProv", state.toUpperCase());
    body.append("submit", "Search");
    resp = await fetch(this.url, {
      method: "POST",
      body
    });
    dom = new JSDOM(await resp.text());
    const error = dom.window.document.querySelector(".alert_msg_error")
      .textContent;
    if (error) {
      if (error.match(/no parking tickets were found/)) {
        return [];
      }
      throw Error(error);
    }

    const citationsElement = dom.window.document.querySelector(
      "script:not(:empty)"
    );
    const citations = JSON.parse(
      citationsElement.textContent.split(";")[0].split("=")[1]
    );
    return citations;
  }
}

module.exports = DsPaymentsLookup;
