const FfxLookup = require(".");

describe("Fairfax Lookup", () => {
  it("return citations with the right keys for VA:DMV2UMD", async () => {
    const ffx = new FfxLookup();
    const citations = await ffx.lookup("VA", "DMV2UMD");
    expect(Object.keys(citations[0])).toEqual([
      "MoneyFields",
      "DisplayFields",
      "ParkciteUniqueKey",
      "AgencyDesignator",
      "LicensePlateNumber",
      "LicStateProv",
      "LicPlateType",
      "AmountDue",
      "IssueNo",
      "RecStatus",
      "ClosedStatus",
      "WentToCollections",
      "IssueDate",
      "IsPayable",
      "SubAgency",
      "ConvenienceFee",
      "ApCollectionKey",
      "VoidReason",
      "ActiveBoot",
      "TrialCode",
      "InCollections",
      "IssueType",
      "Limited",
      "TotalAmountDue",
      "DueDate",
      "LastPaymentDate",
      "ROLastName",
      "IsMilitary",
      "WaiveFee",
      "WaiveIndigent",
      "TripPopupHtml",
      "WaivedFeeForMilitary",
      "WaivedFeeForState",
      "WaivedFeeForDebit",
      "WaivedFeeForIndigent",
      "ConvienceFeeCharged"
    ]);
  });
});
