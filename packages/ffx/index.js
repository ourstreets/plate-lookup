const DsPaymentsLookup = require("@ourstreets/plate-lookup-dspayments");

/**
 * Fairfax Plate lookup service
 * @see {@link ../dspayments|@ourstreets/plate-lookup-dspayments} for more info.
 */
class FfxLookup extends DsPaymentsLookup {
  /**
   * Create an instance of the Fairfax Plate lookup service
   */
  constructor() {
    super("https://dspayments.com/Fairfax");
  }
}

module.exports = FfxLookup;
