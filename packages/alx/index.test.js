const AlxLookup = require(".");

describe("Alexandria Lookup", () => {
  it("return citations with the right keys for MD:5CE5083", async () => {
    const alx = new AlxLookup();
    const citations = await alx.lookup("MD", "5CE5083");
    expect(Object.keys(citations[0])).toEqual([
      "MoneyFields",
      "DisplayFields",
      "ParkciteUniqueKey",
      "AgencyDesignator",
      "LicensePlateNumber",
      "LicStateProv",
      "LicPlateType",
      "AmountDue",
      "IssueNo",
      "RecStatus",
      "ClosedStatus",
      "WentToCollections",
      "IssueDate",
      "IsPayable",
      "SubAgency",
      "ConvenienceFee",
      "ApCollectionKey",
      "VoidReason",
      "ActiveBoot",
      "TrialCode",
      "InCollections",
      "IssueType",
      "Limited",
      "TotalAmountDue",
      "DueDate",
      "LastPaymentDate",
      "ROLastName",
      "IsMilitary",
      "WaiveFee",
      "WaiveIndigent",
      "TripPopupHtml",
      "WaivedFeeForMilitary",
      "WaivedFeeForState",
      "WaivedFeeForDebit",
      "WaivedFeeForIndigent",
      "ConvienceFeeCharged"
    ]);
  });
});
