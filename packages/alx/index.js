const DsPaymentsLookup = require("@ourstreets/plate-lookup-dspayments");

/**
 * Alexandria Plate lookup service
 * @see {@link ../dspayments|@ourstreets/plate-lookup-dspayments} for more info.
 */
class AlxLookup extends DsPaymentsLookup {
  /**
   * Create an instance of the Alexandria Plate lookup service
   */
  constructor() {
    super("https://dspayments.com/Alexandria");
  }
}

module.exports = AlxLookup;
